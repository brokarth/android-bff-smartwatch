package cl.bsoto.vitalsigns.bffmobile.model.bind.rest;

import java.util.Date;

public class VitalSign {
    private Date creationTime;
    private Worker worker;
    private Location location;

    public VitalSign(Date creationTime, Worker worker, Location location) {
        this.creationTime = creationTime;
        this.worker = worker;
        this.location = location;
    }

    public Worker getWorker() {
        return worker;
    }

    public Location getLocation() {
        return location;
    }

    public Date getCreationTime() {
        return creationTime;
    }
}
