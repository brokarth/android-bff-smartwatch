package cl.bsoto.vitalsigns.bffmobile.model.bind.persistence;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class OxygenSaturationPersistence {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Date creationTime;
    private int oxygenSaturationValue;

    @ManyToOne()
    @JoinColumn(name = "WORKER_RUT")
    private WorkerPersistence worker;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private LocationPersistence location;

    public OxygenSaturationPersistence() {
    }

    public OxygenSaturationPersistence(Date creationTime, int oxygenSaturationValue, WorkerPersistence worker, LocationPersistence location) {
        this.creationTime = creationTime;
        this.oxygenSaturationValue = oxygenSaturationValue;
        this.worker = worker;
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public int getOxygenSaturationValue() {
        return oxygenSaturationValue;
    }

    public WorkerPersistence getWorker() {
        return worker;
    }

    public LocationPersistence getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "OxygenSaturationPersistence{" +
                "id=" + id +
                ", creationTime=" + creationTime +
                ", oxygenSaturationValue=" + oxygenSaturationValue +
                ", worker=" + worker +
                ", location=" + location +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OxygenSaturationPersistence that = (OxygenSaturationPersistence) o;
        return id == that.id &&
                oxygenSaturationValue == that.oxygenSaturationValue &&
                Objects.equals(creationTime, that.creationTime) &&
                Objects.equals(worker, that.worker) &&
                Objects.equals(location, that.location);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, creationTime, oxygenSaturationValue, worker, location);
    }
}
