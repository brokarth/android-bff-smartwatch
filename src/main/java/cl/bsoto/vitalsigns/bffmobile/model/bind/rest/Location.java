package cl.bsoto.vitalsigns.bffmobile.model.bind.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {
    private int latitude;
    private int longitude;

    @JsonCreator
    public Location(@JsonProperty("latitude") int latitude, @JsonProperty("longitude") int longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getLongitude() {
        return longitude;
    }
}
