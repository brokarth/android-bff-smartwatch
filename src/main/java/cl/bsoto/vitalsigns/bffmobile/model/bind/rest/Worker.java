package cl.bsoto.vitalsigns.bffmobile.model.bind.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Worker {
    private int rut;
    private String name;

    @JsonCreator
    public Worker(@JsonProperty("rut") int rut, @JsonProperty("name") String name) {
        this.rut = rut;
        this.name = name;
    }

    public int getRut() {
        return rut;
    }

    public String getName() {
        return name;
    }
}
