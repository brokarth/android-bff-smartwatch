package cl.bsoto.vitalsigns.bffmobile.model.bind.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class LocationPersistence {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int latitude;
    private int longitude;

    public LocationPersistence() {
    }

    public LocationPersistence(int latitude, int longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "LocationPersistence{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationPersistence that = (LocationPersistence) o;
        return id == that.id &&
                latitude == that.latitude &&
                longitude == that.longitude;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, latitude, longitude);
    }
}
