package cl.bsoto.vitalsigns.bffmobile.model.bind.rest;

import java.util.Date;

public class BloodPressure extends VitalSign {
    private int systolicPressure;
    private int diastolicPressure;

    public BloodPressure(Date creationTime, int systolicPressure, int diastolicPressure, Worker worker, Location location) {
        super(creationTime, worker, location);
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
    }

    public int getDiastolicPressure() {
        return diastolicPressure;
    }

    public int getSystolicPressure() {
        return systolicPressure;
    }

    @Override
    public String toString() {
        return String.format("Presion Sanguinea: %s/%s", this.systolicPressure, this.diastolicPressure);
    }
}
