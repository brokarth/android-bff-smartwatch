package cl.bsoto.vitalsigns.bffmobile.model.bind.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class WorkerPersistence {
    @Id
    private int rut;
    private String name;

    public WorkerPersistence() {
    }

    public WorkerPersistence(int rut, String name) {
        this.rut = rut;
        this.name = name;
    }


    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "WorkerPersistence{" +
                "rut=" + rut +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorkerPersistence that = (WorkerPersistence) o;
        return rut == that.rut &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rut, name);
    }
}
