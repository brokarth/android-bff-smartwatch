package cl.bsoto.vitalsigns.bffmobile.model.bind.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class OxygenSaturation extends VitalSign {
    private int oxygenSaturationValue;

    @JsonCreator
    public OxygenSaturation(@JsonProperty("creationTime") Date creationTime,
                            @JsonProperty("oxygenSaturationValue") int oxygenSaturationValue,
                            @JsonProperty("worker") Worker worker,
                            @JsonProperty("location") Location location) {
        super(creationTime, worker, location);
        this.oxygenSaturationValue = oxygenSaturationValue;
    }

    public int getOxygenSaturationValue() {
        return oxygenSaturationValue;
    }

    @Override
    public String toString() {
        return String.format("Saturacion de Oxigeno: %s", this.oxygenSaturationValue);
    }
}
