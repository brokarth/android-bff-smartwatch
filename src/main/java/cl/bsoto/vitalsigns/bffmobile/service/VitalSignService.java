package cl.bsoto.vitalsigns.bffmobile.service;

import cl.bsoto.vitalsigns.bffmobile.client.EmailClient;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.LocationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.OxygenSaturationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Location;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.repository.OxygenSaturationRepository;
import cl.bsoto.vitalsigns.bffmobile.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VitalSignService {
    @Value("${app.api.list-hour-limit}")
    private int AMOUNT_HOURS_TO_GET_DATA;

    @Value("${app.vital-sign.warn-oxigen-limit}")
    private int WARN_OXYGEN_LIMIT;

    @Value("${app.vital-sign.emergency-oxygen-limit}")
    private int EMERGENCY_OXYGEN_LIMIT;


    private OxygenSaturationRepository oxygenSaturationRepository;
    private WorkerRepository workerRepository;
    private EmailClient emailClient;

    @Autowired
    VitalSignService(OxygenSaturationRepository oxygenSaturationRepository, WorkerRepository workerRepository, EmailClient emailClient) {
        this.oxygenSaturationRepository = oxygenSaturationRepository;
        this.workerRepository = workerRepository;
        this.emailClient = emailClient;
    }

    public void add(OxygenSaturation oxygenSaturation) {
        checkVitalSign(oxygenSaturation);

        WorkerPersistence workerPersistence = buildWorkerPersistenceFromWorker(oxygenSaturation.getWorker());
        LocationPersistence locationPersistence = buildLocationPersistenceFromLocation(oxygenSaturation.getLocation());
        OxygenSaturationPersistence oxygenSaturationPersistence = buildOxygenSaturationPersistenceFromOxygenSaturationPersistence(oxygenSaturation, workerPersistence, locationPersistence);

        oxygenSaturationRepository.save(oxygenSaturationPersistence);
    }

    private void checkVitalSign(OxygenSaturation oxygenSaturation) {
        if (WARN_OXYGEN_LIMIT < oxygenSaturation.getOxygenSaturationValue()) {
            return;
        }

        if (EMERGENCY_OXYGEN_LIMIT < oxygenSaturation.getOxygenSaturationValue() && oxygenSaturation.getOxygenSaturationValue() <= WARN_OXYGEN_LIMIT) {
            System.out.println("Cuidado, una persona esta con bajo oxygeno");
            return;
        }

        emailClient.sendEmail(String.format("EMERGENCIA, TRABAJADOR CON BAJO OXIGENO: %s", oxygenSaturation.getWorker().getName()));
        System.out.println("EMERGENCIA, ENVIANDO CORREO");
    }

    private OxygenSaturationPersistence buildOxygenSaturationPersistenceFromOxygenSaturationPersistence(OxygenSaturation oxygenSaturation, WorkerPersistence workerPersistence, LocationPersistence locationPersistence) {
        return new OxygenSaturationPersistence(oxygenSaturation.getCreationTime(), oxygenSaturation.getOxygenSaturationValue(), workerPersistence, locationPersistence);
    }

    private WorkerPersistence buildWorkerPersistenceFromWorker(Worker worker) {
        return workerRepository.findById(worker.getRut()).orElse(workerRepository.save(new WorkerPersistence(worker.getRut(), worker.getName())));
    }

    private LocationPersistence buildLocationPersistenceFromLocation(Location location) {
        return new LocationPersistence(location.getLatitude(), location.getLongitude());
    }

    public List<OxygenSaturation> getFromLastHour() {
        final Date lastHour = getLastHour();

        return convertFromPersistenceObjectToRestObject(oxygenSaturationRepository.findByCreationTimeGreaterThan(lastHour));
    }

    private List<OxygenSaturation> convertFromPersistenceObjectToRestObject(List<OxygenSaturationPersistence> oxygenSaturationPersistenceList) {
        return oxygenSaturationPersistenceList.stream().map(oxygenSaturationPersistence -> {
            Worker worker = convertFromPersistenceObjectToRestObject(oxygenSaturationPersistence.getWorker());
            Location location = convertFromPersistenceObjectToRestObject(oxygenSaturationPersistence.getLocation());
            return new OxygenSaturation(oxygenSaturationPersistence.getCreationTime(), oxygenSaturationPersistence.getOxygenSaturationValue(), worker, location);
        }).collect(Collectors.toList());
    }

    private Location convertFromPersistenceObjectToRestObject(LocationPersistence locationPersistence) {
        return new Location(locationPersistence.getLatitude(), locationPersistence.getLongitude());
    }

    private Worker convertFromPersistenceObjectToRestObject(WorkerPersistence workerPersistence) {
        return new Worker(workerPersistence.getRut(), workerPersistence.getName());
    }

    public List<OxygenSaturation> getFromLastHourByWorker(int rut) {
        final WorkerPersistence workerPersistence = workerRepository.findById(rut).orElseThrow(() -> new RuntimeException("Worker not found"));
        final Date lastHour = getLastHour();
        return convertFromPersistenceObjectToRestObject(oxygenSaturationRepository.findByCreationTimeGreaterThanAndWorker(lastHour, workerPersistence));
    }

    private Date getLastHour() {
        return new Date(System.currentTimeMillis() - 3600 * 1000 * AMOUNT_HOURS_TO_GET_DATA);
    }
}
