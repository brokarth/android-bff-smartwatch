package cl.bsoto.vitalsigns.bffmobile.service;

import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.LocationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.OxygenSaturationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Location;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.repository.OxygenSaturationRepository;
import cl.bsoto.vitalsigns.bffmobile.repository.WorkerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkerService {
    private final OxygenSaturationRepository oxygenSaturationRepository;
    private final WorkerRepository workerRepository;

    @Autowired
    public WorkerService(OxygenSaturationRepository oxygenSaturationRepository, WorkerRepository workerRepository) {
        this.oxygenSaturationRepository = oxygenSaturationRepository;
        this.workerRepository = workerRepository;
    }

    public List<OxygenSaturation> getSignsVitalsFromWorker(int workerRut) {
        WorkerPersistence workerPersistence = getWorkerFromDataBase(workerRut);
        return convertFromPersistenceObjectToRestObject(oxygenSaturationRepository.findByWorker(workerPersistence));
    }

    private WorkerPersistence getWorkerFromDataBase(int workerRut) {
        return workerRepository.findById(workerRut).orElseThrow(() -> new RuntimeException("Invalid worker rut"));
    }

    private List<OxygenSaturation> convertFromPersistenceObjectToRestObject(List<OxygenSaturationPersistence> oxygenSaturationPersistences) {
        return oxygenSaturationPersistences.stream().map(oxygenSaturationPersistence -> {
            Worker worker = convertFromPersistenceObjectToRestObject(oxygenSaturationPersistence.getWorker());
            Location location = convertFromPersistenceObjectToRestObject(oxygenSaturationPersistence.getLocation());
            return new OxygenSaturation(oxygenSaturationPersistence.getCreationTime(), oxygenSaturationPersistence.getOxygenSaturationValue(), worker, location);
        }).collect(Collectors.toList());
    }

    private Location convertFromPersistenceObjectToRestObject(LocationPersistence locationPersistence) {
        return new Location(locationPersistence.getLatitude(), locationPersistence.getLongitude());
    }

    private Worker convertFromPersistenceObjectToRestObject(WorkerPersistence workerPersistence) {
        return new Worker(workerPersistence.getRut(), workerPersistence.getName());
    }

    public List<Worker> getWorkersByName(String name) {
        List<WorkerPersistence> workersPersistence = workerRepository.findByNameStartsWith(name);
        return workersPersistence.stream().map(workerPersistence -> new Worker(workerPersistence.getRut(), workerPersistence.getName())).collect(Collectors.toList());
    }

    public Worker getWorkerByRut(int rut) {
        WorkerPersistence workerPersistence = workerRepository.findById(rut).orElseThrow(() -> new RuntimeException("Worker not found"));
        return new Worker(workerPersistence.getRut(), workerPersistence.getName());
    }
}
