package cl.bsoto.vitalsigns.bffmobile.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailClient {
    @Value("${app.email.to}")
    private String[] TO;

    @Value("${app.email.subject}")
    private String SUBJECT;

    @Autowired
    private JavaMailSender sender;


    public void sendEmail(String body) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(TO);
            helper.setText(body);
            helper.setSubject(SUBJECT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        sender.send(message);
    }
}
