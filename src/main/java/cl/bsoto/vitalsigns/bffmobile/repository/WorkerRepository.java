package cl.bsoto.vitalsigns.bffmobile.repository;

import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkerRepository extends CrudRepository<WorkerPersistence, Integer> {
    List<WorkerPersistence> findByNameStartsWith(String name);
}
