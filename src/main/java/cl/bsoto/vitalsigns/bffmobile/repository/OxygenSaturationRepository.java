package cl.bsoto.vitalsigns.bffmobile.repository;

import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.OxygenSaturationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OxygenSaturationRepository extends CrudRepository<OxygenSaturationPersistence, Long> {
    List<OxygenSaturationPersistence> findByWorker(WorkerPersistence workerPersistence);

    List<OxygenSaturationPersistence> findByCreationTimeGreaterThan(Date startDate);

    List<OxygenSaturationPersistence> findByCreationTimeGreaterThanAndWorker(Date any, WorkerPersistence worker);
}
