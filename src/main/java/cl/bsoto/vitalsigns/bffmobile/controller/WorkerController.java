package cl.bsoto.vitalsigns.bffmobile.controller;

import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("worker")
public class WorkerController {
    private WorkerService workerService;

    @Autowired
    public WorkerController(WorkerService workerService) {
        this.workerService = workerService;
    }

    @GetMapping("{workerRut}/oxygen-saturation")
    public List<OxygenSaturation> getVitalSigns(@PathVariable("workerRut") int workerRut) {
        return workerService.getSignsVitalsFromWorker(workerRut);
    }

    @GetMapping("by-name/{name}")
    public List<Worker> getWorkerByName(@PathVariable("name") String name) {
        return workerService.getWorkersByName(name);
    }

    @GetMapping("by-rut/{rut}")
    public Worker getWorkerByRut(@PathVariable("rut") int rut) {
        return workerService.getWorkerByRut(rut);
    }
}
