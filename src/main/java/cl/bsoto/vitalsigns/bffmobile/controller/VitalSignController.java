package cl.bsoto.vitalsigns.bffmobile.controller;

import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.service.VitalSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("vital-sign")
public class VitalSignController {
    private VitalSignService vitalSignService;

    @Autowired
    public VitalSignController(VitalSignService vitalSignService) {
        this.vitalSignService = vitalSignService;
    }

    @PostMapping("/oxygen-saturation")
    public void addOxygenSaturation(@RequestBody OxygenSaturation oxygenSaturation) {
        this.vitalSignService.add(oxygenSaturation);
    }

    @GetMapping("/")
    public List<OxygenSaturation> getLastHourOxygenVitalSigns() {
        return this.vitalSignService.getFromLastHour();
    }


    @GetMapping("/{workerRut}")
    public List<OxygenSaturation> getLastHourOxygenVitalSignsByWorker(@PathVariable int workerRut) {
        return this.vitalSignService.getFromLastHourByWorker(workerRut);
    }
}
