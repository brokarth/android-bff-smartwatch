package cl.bsoto.vitalsigns.bffmobile.service;

import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.LocationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.OxygenSaturationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.repository.OxygenSaturationRepository;
import cl.bsoto.vitalsigns.bffmobile.repository.WorkerRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class WorkerServiceTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    private WorkerService workerService;
    private OxygenSaturationRepository oxygenSaturationRepository;
    private WorkerRepository workerRepository;

    @Before
    public void setUp() {
        oxygenSaturationRepository = mock(OxygenSaturationRepository.class);
        workerRepository = mock(WorkerRepository.class);

        workerService = new WorkerService(oxygenSaturationRepository, workerRepository);
    }

    private LocationPersistence buildTestLocation() {
        return new LocationPersistence(0, 0);
    }

    private WorkerPersistence buildTestWorker() {
        return new WorkerPersistence(11111111, "Test");
    }

    @Test
    public void shouldGetAllVitalsSignsFromWorker() {
        final LocationPersistence locationPersistence = buildTestLocation();
        final WorkerPersistence workerPersistence = buildTestWorker();
        List<OxygenSaturationPersistence> vitalSignsFromBd = Arrays.asList(new OxygenSaturationPersistence(new Date(), 98, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 99, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 100, workerPersistence, locationPersistence));
        when(oxygenSaturationRepository.findByWorker(workerPersistence)).thenReturn(vitalSignsFromBd);
        when(workerRepository.findById(workerPersistence.getRut())).thenReturn(Optional.of(workerPersistence));

        List<OxygenSaturation> vitalSignsResult = workerService.getSignsVitalsFromWorker(workerPersistence.getRut());

        verify(oxygenSaturationRepository, times(1)).findByWorker(eq(workerPersistence));
        verify(workerRepository, times(1)).findById(eq(workerPersistence.getRut()));
        assertEquals(vitalSignsResult.size(), 3);
    }

    @Test
    public void shouldThrowRuntimeExceptionWhenInvalidWorker() {
        final int invalidRut = 11111111;
        when(workerRepository.findById(invalidRut)).thenReturn(Optional.ofNullable(null));

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage("Invalid worker rut");

        workerService.getSignsVitalsFromWorker(invalidRut);
    }

    @Test
    public void shouldReturnWorkersByName() {
        final Worker expectedWorker = new Worker(11111111, "Test");
        when(workerRepository.findByNameStartsWith(anyString())).thenReturn(Arrays.asList(new WorkerPersistence(expectedWorker.getRut(), expectedWorker.getName())));

        List<Worker> resultWorkers = workerService.getWorkersByName(expectedWorker.getName());

        assertEquals(1, resultWorkers.size());

        Worker resultWorker = resultWorkers.get(0);
        assertEquals(expectedWorker.getRut(), resultWorker.getRut());
        assertEquals(expectedWorker.getName(), resultWorker.getName());
    }

    @Test
    public void shouldReturnWorkerByRut() {
        final Worker expectedWorker = new Worker(11111111, "Test");
        when(workerRepository.findById(anyInt())).thenReturn(Optional.of(new WorkerPersistence(expectedWorker.getRut(), expectedWorker.getName())));

        Worker resultWorker = workerService.getWorkerByRut(expectedWorker.getRut());

        assertEquals(expectedWorker.getRut(), resultWorker.getRut());
        assertEquals(expectedWorker.getName(), resultWorker.getName());
    }

    @Test
    public void shouldThrowRuntimeExceptionWhenRutInvalid() {
        final int invalidRut = 11111111;
        when(workerRepository.findById(invalidRut)).thenReturn(Optional.ofNullable(null));

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage("Worker not found");

        workerService.getWorkerByRut(invalidRut);
    }

}