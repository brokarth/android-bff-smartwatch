package cl.bsoto.vitalsigns.bffmobile.service;

import cl.bsoto.vitalsigns.bffmobile.client.EmailClient;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.LocationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.OxygenSaturationPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.persistence.WorkerPersistence;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Location;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.repository.OxygenSaturationRepository;
import cl.bsoto.vitalsigns.bffmobile.repository.WorkerRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class VitalSignServiceTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    private VitalSignService vitalSignService;
    private OxygenSaturationRepository oxygenSaturationRepository;
    private WorkerRepository workerRepository;
    private EmailClient emailClient;

    @Before
    public void setUp() {
        oxygenSaturationRepository = mock(OxygenSaturationRepository.class);
        workerRepository = mock(WorkerRepository.class);
        emailClient = mock(EmailClient.class);

        vitalSignService = new VitalSignService(oxygenSaturationRepository, workerRepository, emailClient);
    }

    private Location buildTestLocation() {
        return new Location(0, 0);
    }

    private Worker buildTestWorker() {
        return new Worker(11111111, "Test");
    }

    @Test
    public void shouldCallDataBaseClientWithOxygenVitalSign() {
        final Location location = buildTestLocation();
        final Worker worker = buildTestWorker();
        final OxygenSaturation oxygenSaturation = new OxygenSaturation(new Date(), 90, worker, location);

        final LocationPersistence locationPersistence = new LocationPersistence(location.getLatitude(), location.getLongitude());
        final WorkerPersistence workerPersistence = new WorkerPersistence(worker.getRut(), worker.getName());
        final OxygenSaturationPersistence oxygenSaturationPersistence = new OxygenSaturationPersistence(oxygenSaturation.getCreationTime(), oxygenSaturation.getOxygenSaturationValue(), workerPersistence, locationPersistence);

        when(workerRepository.findById(anyInt())).thenReturn(Optional.of(workerPersistence));

        vitalSignService.add(oxygenSaturation);

        verify(oxygenSaturationRepository, times(1)).save(eq(oxygenSaturationPersistence));
    }

    @Test
    public void shouldSendEmailWhenOxygenVitalSignIsLow60() {
        final Location location = buildTestLocation();
        final Worker worker = buildTestWorker();
        final OxygenSaturation oxygenSaturation = new OxygenSaturation(new Date(), 60, worker, location);

        ReflectionTestUtils.setField(vitalSignService, "WARN_OXYGEN_LIMIT", 70);
        ReflectionTestUtils.setField(vitalSignService, "EMERGENCY_OXYGEN_LIMIT", 60);

        vitalSignService.add(oxygenSaturation);

        verify(emailClient, times(1)).sendEmail(anyString());
    }

    @Test
    public void shouldGetOxygenVitalSignFromTheLastHour() {
        final LocationPersistence locationPersistence = new LocationPersistence(0, 0);
        final WorkerPersistence workerPersistence = new WorkerPersistence(11111111, "Test");
        final List<OxygenSaturationPersistence> oxygenSaturationPersistenceList = Arrays.asList(new OxygenSaturationPersistence(new Date(), 98, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 99, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 100, workerPersistence, locationPersistence));
        when(oxygenSaturationRepository.findByCreationTimeGreaterThan(any(Date.class))).thenReturn(oxygenSaturationPersistenceList);

        List<OxygenSaturation> oxygenSaturationList = vitalSignService.getFromLastHour();

        assertEquals(oxygenSaturationPersistenceList.size(), oxygenSaturationList.size());
    }

    @Test
    public void shouldGetOxygenVitalSignFromTheLastHourByWorker() {
        final LocationPersistence locationPersistence = new LocationPersistence(0, 0);
        final WorkerPersistence workerPersistence = new WorkerPersistence(11111111, "Test");
        final List<OxygenSaturationPersistence> oxygenSaturationPersistenceList = Arrays.asList(new OxygenSaturationPersistence(new Date(), 98, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 99, workerPersistence, locationPersistence),
                new OxygenSaturationPersistence(new Date(), 100, workerPersistence, locationPersistence));
        when(workerRepository.findById(anyInt())).thenReturn(Optional.of(workerPersistence));
        when(oxygenSaturationRepository.findByCreationTimeGreaterThanAndWorker(any(Date.class), any(WorkerPersistence.class))).thenReturn(oxygenSaturationPersistenceList);

        List<OxygenSaturation> oxygenSaturationList = vitalSignService.getFromLastHourByWorker(workerPersistence.getRut());

        assertEquals(oxygenSaturationPersistenceList.size(), oxygenSaturationList.size());
    }

    @Test
    public void shouldThrowRuntimeExceptionWhenInvalidWorker() {
        final int invalidRut = 11111111;
        when(workerRepository.findById(invalidRut)).thenReturn(Optional.ofNullable(null));

        expectedEx.expect(RuntimeException.class);
        expectedEx.expectMessage("Worker not found");

        vitalSignService.getFromLastHourByWorker(invalidRut);
    }
}