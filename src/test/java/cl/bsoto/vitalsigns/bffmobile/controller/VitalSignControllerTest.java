package cl.bsoto.vitalsigns.bffmobile.controller;

import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Location;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.service.VitalSignService;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class VitalSignControllerTest {
    private VitalSignController vitalSignController;
    private VitalSignService vitalSignService;

    @Before
    public void setUp() {
        vitalSignService = mock(VitalSignService.class);
        vitalSignController = new VitalSignController(vitalSignService);
    }

    private Location buildTestLocation() {
        return new Location(0, 0);
    }

    private Worker buildTestWorker() {
        return new Worker(11111111, "Test");
    }

    @Test
    public void shouldCallBusinessServiceWithOxygenVitalSign() {
        final Location location = buildTestLocation();
        final Worker worker = buildTestWorker();
        final OxygenSaturation oxygenSaturation = new OxygenSaturation(new Date(), 90, worker, location);

        vitalSignController.addOxygenSaturation(oxygenSaturation);

        verify(vitalSignService, times(1)).add(eq(oxygenSaturation));
    }

    @Test
    public void shouldCallBusinessServiceToGetSignsFromLasHour() {
        final Location location = buildTestLocation();
        final Worker worker = buildTestWorker();
        final OxygenSaturation oxygenSaturation = new OxygenSaturation(new Date(), 90, worker, location);

        vitalSignController.getLastHourOxygenVitalSigns();

        verify(vitalSignService, times(1)).getFromLastHour();
    }

    @Test
    public void shouldCallBusinessServiceToGetSignsFromLasHourByWorker() {
        final Location location = buildTestLocation();
        final Worker worker = buildTestWorker();
        final OxygenSaturation oxygenSaturation = new OxygenSaturation(new Date(), 90, worker, location);

        vitalSignController.getLastHourOxygenVitalSignsByWorker(worker.getRut());

        verify(vitalSignService, times(1)).getFromLastHourByWorker(anyInt());
    }

}