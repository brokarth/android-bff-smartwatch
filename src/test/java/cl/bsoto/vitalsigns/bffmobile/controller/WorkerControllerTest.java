package cl.bsoto.vitalsigns.bffmobile.controller;

import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Location;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.OxygenSaturation;
import cl.bsoto.vitalsigns.bffmobile.model.bind.rest.Worker;
import cl.bsoto.vitalsigns.bffmobile.service.WorkerService;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class WorkerControllerTest {
    private WorkerController workerController;
    private WorkerService workerService;

    @Before
    public void setUp() {
        workerService = mock(WorkerService.class);
        workerController = new WorkerController(workerService);
    }

    @Test
    public void shouldCallBusinessServiceToGetOxygenSaturationFromWorker() {
        Worker worker = new Worker(11111111, "Test");
        Location location = new Location(0, 0);
        List<OxygenSaturation> expectedVitalSigns = Arrays.asList(new OxygenSaturation(new Date(), 98, worker, location),
                new OxygenSaturation(new Date(), 99, worker, location),
                new OxygenSaturation(new Date(), 99, worker, location));
        when(workerService.getSignsVitalsFromWorker(anyInt())).thenReturn(expectedVitalSigns);

        List<OxygenSaturation> vitalSignsResult = workerController.getVitalSigns(worker.getRut());

        verify(workerService, times(1)).getSignsVitalsFromWorker(eq(worker.getRut()));
        assertEquals(expectedVitalSigns, vitalSignsResult);
    }

    @Test
    public void shouldCallBusinessServiceToGetWorkerByName() {
        final Worker expectedWorker = new Worker(11111111, "Test");
        when(workerService.getWorkersByName(anyString())).thenReturn(Arrays.asList(expectedWorker));

        List<Worker> resultWorkers = workerController.getWorkerByName(expectedWorker.getName());

        assertEquals(1, resultWorkers.size());

        Worker resultWorker = resultWorkers.get(0);
        assertEquals(expectedWorker.getRut(), resultWorker.getRut());
        assertEquals(expectedWorker.getName(), resultWorker.getName());
    }

    @Test
    public void shouldCallBusinessServiceToGetWorkerByRut() {
        final Worker expectedWorker = new Worker(11111111, "Test");
        when(workerService.getWorkerByRut(anyInt())).thenReturn(expectedWorker);

        Worker resultWorker = workerController.getWorkerByRut(expectedWorker.getRut());

        assertEquals(expectedWorker.getRut(), resultWorker.getRut());
        assertEquals(expectedWorker.getName(), resultWorker.getName());
    }
}