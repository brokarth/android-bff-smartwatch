FROM openjdk:8-jre

WORKDIR /app
COPY . .
RUN ./gradlew bootRun
